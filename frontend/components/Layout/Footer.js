import contacts from '../../data/contacts';

export const Footer = () => (
  <footer>
    <section>
      <div className="container">
        <div className="footer_content">
          <div className="footer_content_block">
            <h4>Наши контакты</h4>
            <p>
              <a href={`tel:${contacts.phone}`}>{contacts.phone}</a>
            </p>
            <p>
              <a href={`mailto:${contacts.email}`} className="link">{contacts.email}</a>
            </p>
            <p>{contacts.company}</p>
            <p>{contacts.inn}</p>
          </div>
          <div className="footer_content_block">
            <h4>Наш адрес</h4>
            <p>
              {contacts.address}
            </p>
          </div>
          <div className="footer_content_block">
            <h4>Время работы</h4>
            {contacts.wordTimes.map((e, i) => (<p key={i}>{e}</p>))}
          </div>
          <div className="footer_content_block social">
            <h4>Социальные сети</h4>
            {contacts.social.map((e) => (
              <p key={e.id}>
                {e.icon}
                <a
                  href={e.link}
                  target="_blank"
                  rel="noreferrer"
                >
                  {e.name}
                </a>
              </p>
            ))}
          </div>
        </div>
      </div>
    </section>
    <div className="footer_down">
      <div className="container">
        <div className="footer_block">
          <p>© 2020 Все права защищены законодательством РФ. </p>
          <p>
            <a className="href">Политика конфиденциальности</a>
          </p>
        </div>
      </div>
    </div>
  </footer>
);
