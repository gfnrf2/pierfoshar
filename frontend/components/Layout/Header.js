import {LogoIcon, MenuIcon, CalendarIcon, PinIcon, PhoneIcon, WhatsappIcon} from '../Icons';

import menuItems from '../../data/menuItems';

const Logo = () => (
  <div className="logo">
    <a href="./" className="logo">
      <LogoIcon />
    </a>
  </div>
);

const Location = () => (
  <div className="location">
    <div className="icon">
      <PinIcon />
    </div>
    <div>
      <p>
        Ст. м. Технологический институт,
      </p>
      <p>
        Серпуховская ул., 14
      </p>
    </div>
  </div>
);

const Contacts = () => (
  <div className="contacts">
    <div className="phone">
      <PhoneIcon />
      <WhatsappIcon />
      <a href="tel:+7 (812) 309-06-62">
        +7 (812)
        <span> 309-06-62</span>
      </a>
    </div>
    <div className="whatsapp">
      <WhatsappIcon />
      <a href="#!">Whatsapp</a>
    </div>
  </div>
);

export const Header = ({ onClick }) => (
  <header>
    <div className="container">
      <div className="top">
        <Logo />
        <Location />
        <Contacts />
        <button type="button" className="menu_btn">
          <MenuIcon />
        </button>
        <div className="recording">
          <button type="button" className="btn sing_up" onClick={onClick}>
            <CalendarIcon />
            Запись на прием
          </button>
        </div>
      </div>
      <nav>
        <ul>
          {menuItems.map((item) => (
            <li key={item.id}>
              <a href={item.href}>{item.title}</a>
            </li>
          ))}
        </ul>
      </nav>
    </div>
  </header>
);
