import Image from 'next/image';

export const AdvantagesSection = () => (
  <section className="advantages" style={{ backgroundImage: 'url(img/dist/advantages.svg), url(img/dist/map.svg)' }}>
    <div>
      <div className="advantages_content">
        <div className="title">
          <h2>У нас есть все необходимое, чтобы создавать Вам новые улыбки</h2>
          <p>/ Наши врачи обучаются на лучших семинарах /</p>
          <div className="border" />
        </div>
        <div className="advantages_block">
          <div className="advantages_elem">
            <img src="img/dist/advantages/doctor.webp" alt="img" className="advantages_img" />
            <h4>Опытные врачи</h4>
            <p>
              Наши врачи обучаются на
              лучших семинарах у ведущих
              специалистов. Постоянно повышая свои навыки и следуя современным тенденциям лечения.
            </p>
          </div>
          <div className="advantages_elem">
            <img src="img/dist/advantages/micro.webp" alt="img" className="advantages_img" />
            <h4>Работа с увеличением</h4>
            <p>
              Все хирургические манипуляции
              врач проводит с помощью
              увеличительной оптики , что в
              разы увеличивает точность и
              безопасность лечения.
            </p>
          </div>
          <div className="advantages_elem">
            <img src="img/dist/advantages/3d.webp" alt="img" className="advantages_img" />
            <h4>Интраоральный 3D сканер</h4>
            <p>
              Предельная точность, компьютерного
              сканирования, возможность
              моделирования будущего результата. Расчет и прогнозирование различных
              вариантов конструкций, а так же
              навигационная хирургия .
            </p>
          </div>
        </div>
      </div>
      <div className="location_content">
        <div className="location_image">
          <div className="location_slider slick-initialized slick-slider" data-count="">
            <button
              className="slick-prev slick-arrow slick-disabled"
              aria-label="Previous"
              type="button"
              aria-disabled="true"
            >
              Previous
            </button>
            <div className="slick-list draggable">
              <div className="slick-track" style={{ opacity: '1', width: '3520px', transform: 'translate3d(0, 0, 0)' }}>
                {/*
                     <div className="location_slider_img slick-slide slick-current slick-active"
                       style="background: url(&quot;img/loc1.webp&quot;) center center / cover; width: 440px;"
                       data-slick-index="0" aria-hidden="false" tabIndex="0" />
                  <div className="location_slider_img slick-slide"
                       style="background: url(&quot;img/loc2.webp&quot;) center center / cover; width: 440px;"
                       data-slick-index="1" aria-hidden="true" tabIndex="-1" />
                  <div className="location_slider_img slick-slide"
                       style="background: url(&quot;img/loc3.webp&quot;) center center / cover; width: 440px;"
                       data-slick-index="2" aria-hidden="true" tabIndex="-1" />
                  <div className="location_slider_img slick-slide"
                       style="background: url(&quot;img/loc4.webp&quot;) center center / cover; width: 440px;"
                       data-slick-index="3" aria-hidden="true" tabIndex="-1"></div>
                  <div className="location_slider_img slick-slide"
                       style="background: url(&quot;img/loc5.webp&quot;) center center / cover; width: 440px;"
                       data-slick-index="4" aria-hidden="true" tabIndex="-1"></div>
                  <div className="location_slider_img slick-slide"
                       style="background: url(&quot;img/loc6.webp&quot;) center center / cover; width: 440px;"
                       data-slick-index="5" aria-hidden="true" tabIndex="-1"></div>
                  <div className="location_slider_img slick-slide"
                       style="background: url(&quot;img/loc7.webp&quot;) center center / cover; width: 440px;"
                       data-slick-index="6" aria-hidden="true" tabIndex="-1"></div>
                  <div className="location_slider_img slick-slide"
                       style="background: url(&quot;img/loc8.webp&quot;) center center / cover; width: 440px;"
                       data-slick-index="7" aria-hidden="true" tabIndex="-1"></div>
                   */}
              </div>
            </div>

            <button
              className="slick-next slick-arrow"
              aria-label="Next"
              type="button"
              aria-disabled="false"
            >
              Next
            </button>
          </div>
        </div>
        <div className="title">
          <h2>Где проходит лечение</h2>
          <div className="border" />
          <div className="article">
            <p>
              Клиника Пьер Фошар основана в 2006 г. и находиться в историческом центре города. В клинике 4
              кабинета оборудованных топовыми стоматологическими установками Kavo (Германия), отдельный
              хирургический и рентген-кабинет.
            </p>
          </div>
        </div>
        <div className="slider_count">
          <p>01</p>
          <span>/08</span>
        </div>
        <div className="info_plug">
          <ul>
            <li>Клиника находится в Санкт-Петербурге</li>
          </ul>
          <div className="info_plug_about">
            <img src="img/dist/under.svg" alt="img" className="info_plug_img" />
            <p>
              <span>
                в
                <span className="info_number">3</span>
                {' '}
                мин
              </span>
              &emsp;от метро
              {' '}
              <br />
              “Технологический
              Институт”
            </p>
          </div>

        </div>
      </div>
    </div>
  </section>
);
