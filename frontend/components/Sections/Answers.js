import { useState } from 'react';
import { ArrowIcon, PlayIcon, YouTubeIcon } from '../Icons';

const answers = [
  {
    id: 0,
    title: 'О клинике Пьер Фошар',
    link: 'tmPRB78gTQE',
  },
  {
    id: 1,
    title: 'О врачах клиники Пьер Фошар',
    link: 'HSjc0MHCHNM',
  },
  {
    id: 2,
    title: 'Как выбрать пациенту клинику и не ошибиться?',
    link: 'ThxGHSX2B6o',
  },
  {
    id: 3,
    title: 'Почему пациент должен выбрать клинику Пьер Фошар?',
    link: 'MleT4Lr1VvQ',
  },
  {
    id: 4,
    title: 'Про имплантацию зубов в клинике Пьер Фошар',
    content: 'lBuLx9eCq3g',
  },
  {
    id: 5,
    title: 'Для чего нужен микроскоп в стоматологии?',
    content: 'nq9yZtET3jA',
  },
  {
    id: 6,
    title: 'Что такое цифровая стоматология и для чего она нужна?',
    content: 'FcUf0GUrH8c',
  },
  {
    id: 7,
    title: 'Про виниры в клинике Пьер Фошар',
    content: 'qsVfFj2ba5g',
  },
];

export const AnswersSection = () => {
  const [current, setCurrent] = useState(0);
  const [key, setKey] = useState(answers[0].link);
  const [click, setClick] = useState(false);
  return (
    <section className="answers" style={{ background: 'url(img/dist/answers/bg.png) center/cover' }}>
      <div className="container">
        <div className="answers_block">
          <div className="plug answers_content">
            <div className="title">
              <h2>8 ответов</h2>
              <p>/ на важные вопросы о стоматологии /</p>
              <div className="border" />
            </div>
            <p>Отвечает генеральный директор и главный врач клиники Пьер Фошар – Иванькин Евгений Викторович</p>
            <ul className="answers_list">
              {answers.map((e) => (
                <li
                  key={e.id}
                  onClick={() => {
                    setKey(e.link);
                    setClick(false);
                    setCurrent(e.id);
                  }}
                >
                  <div className="play" >
                    <ArrowIcon />
                  </div>
                  <p>
                    {e.title}
                  </p>
                </li>
              ))}
            </ul>
          </div>
          <div className="plug video_content">
            <div className="for_video" onClick={() => setClick(!click)}>
              <PlayIcon style={click ? { display: 'none' } : { display: 'block' }} />
              <img
                src={`//img.youtube.com/vi/${key}/maxresdefault.jpg`}
                alt="Видео на YouTube"
                style={click ? { display: 'none' } : { display: 'block' }}
              />
              <iframe
                width="99.9%"
                height="262px"
                src={`https://www.youtube.com/embed/${key}`}
                frameBorder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
              />
            </div>

            <button type="button" className="btn sing_up">Задать вопрос</button>
            <a
              href="https://www.youtube.com/channel/UCkIkjZLwKua-_l_2JNSRuPg"
              target="_blank"
              className="youtube_block"
              rel="noreferrer"
            >
              <YouTubeIcon />
              <p>
                Смотрите полную версию
                <br />
                интервью на YOUTUBE
              </p>
            </a>
          </div>
        </div>
      </div>
    </section>
  );
};
