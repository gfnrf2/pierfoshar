import Image from 'next/image';
import appeals from '../../data/appeals';

export const AppealSection = () => {
  const appeal = appeals[0];
  return (
    <section className="appeal" style={{ background: `url(${appeal.bg}) center/cover` }}>
      <div className="appeal_img">
        <Image {...appeal.img} />
      </div>
      <div className="container">
        <div className="plug">
          <div className="title">
            {appeal.title}
            <div className="border" />
          </div>
          <article>
            {appeal.paragraphs.map((e, i) => (<p key={i}>{e}</p>))}
            {appeal.conclusion && (
                <p>
                  <span className="bold">{appeal.conclusion}</span>
                </p>
            )}
          </article>
        </div>
      </div>
    </section>
  );
};
