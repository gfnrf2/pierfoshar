import Image from 'next/image';
import Slider from 'react-slick';
import { useState } from 'react';
import { ArrowIcon } from '../Icons';
import data from '../../data/certificatesSection';

export const CertificatesSection = () => {
  const [current, setCurrent] = useState(0);
  const cert = data.certs[(current + 3) % data.certs.length];

  const NextArrow = ({ onClick, className, style }) => (
    <div onClick={onClick} className={className} style={style}>
      <ArrowIcon/>
    </div>
  );

  const PrevArrow = ({ onClick, className, style }) => (
      <div onClick={onClick} className={className} style={style}>
      <ArrowIcon/>
    </div>
  );

  const settings = {
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    // autoplay: true,
    // autoplaySpeed: 5000,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    slidesToScroll: 1,
    beforeChange: (_, next) => setCurrent(next),
  };

  const SliderCount = () => (
    <div className="slider_count">
      <p>{current + 1}</p>
      <span>
        /
        {data.certs.length}
      </span>
    </div>
  );

  return (
    <>
      <section className="certificates" >
        <div className="container">
          <div className="certificates_block">
            <div className="title">
              <h2>{data.title}</h2>
              <div className="border" />
              <p>
                {data.description}
              </p>
              <SliderCount />
            </div>
            <div className="certificates_image">
              <div className="certificates_slider">
                {cert && (
                  <a className="certificates_slider_elem" href={cert.url} key={cert.id}>
                    <Image {...cert.img} />
                  </a>
                )}
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="certificates_content">
        <div className="wrapper">
          <Slider className="certificates_slider_nav" {...settings}>
            {data.certs.map((e) => (
              <div className="certificates_slider_nav_elem" key={e.id}>
                <Image {...e.img} />
              </div>
            ))}
          </Slider>
        </div>
      </section>
    </>
  );
};
