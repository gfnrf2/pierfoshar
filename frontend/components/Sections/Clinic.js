import Image from 'next/image';
import clinicData from '../../data/clinic';

export const ClinicSection = () => {
  const article = clinicData.articles[0];
  return (
    <section className="clinic">
      <div className="container">
        <div className="img-wrapper">
          <Image src="/img/dist/about/pier.png" width={700} height={500} alt="Пьер Фошар" />
        </div>
        <div className="plug">
          <div className="title">
            <h2>{article.title}</h2>
            <div className="border" />
          </div>
          <article>
            {article.paragraphs.map((e, i) => (<p key={i}>{e}</p>))}
          </article>
        </div>
      </div>
    </section>
  );
};

export const ClinicSecondSection = () => {
  const article = clinicData.articles[1];
  return (
    <section className="clinic second">
      <div className="container">
        <Image src="/img/dist/about/pier2.svg" width={700} height={500} alt="Пьер Фошар" />
        <div className="plug">
          <div className="title">
            <h2>{article.title}</h2>
            <p>{article.subtitle}</p>
            <div className="border" />
          </div>
          <article>
            {article.paragraphs.map((e, i) => (<p key={i}>{e}</p>))}
          </article>
        </div>
      </div>
    </section>
  );
};
