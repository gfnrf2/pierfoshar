import Slider from 'react-slick';
import { useState } from 'react';
import { ArrowIcon, RewardIcon } from '../Icons';

const DoctorCard = ({
  experience, key, src, fullName, position, article,
}) => (
  <div className="doctors_slider_elem" key={key}>
    <div className="doctors_slider_img">
      <img src={src} alt={fullName} />
      <div className="circle" />
    </div>
    <div className="container">
      <div className="experience">
        <p>{experience}</p>
        <p className="small">лет</p>
        <span>опыт работы</span>
      </div>
      <div className="plug">
        <div className="title">
          <h2>{fullName}</h2>
          <p>{position}</p>
          <div className="border" />
        </div>
        <article>
          {article}
        </article>
        <div className="bottom">
          <div className="reward">
            <RewardIcon className="reward_img" />
            <p>
              Профессионализм при решении проблем
              <br />
              {' '}
              любой сложности
            </p>
          </div>
          <button className="btn">Подробнее о враче</button>
        </div>
      </div>
    </div>
  </div>
);

export const DoctorsSection = () => {
  const [current, setCurrent] = useState(0);
  const doctors = [
    {
      srcSmall: 'img/dist/doctors/1.png',
      src: 'img/dist/doctors/1b.png',
      experience: 10,
      fullName: 'Евгений Викторович Иванькин',
      name: 'Евгений',
      surname: 'Иванькин',
      middleName: 'Викторович',
      position: '/ главный врач / стоматолог-хирург / стоматолог-ортопед/',
      article: (
        <>
          <p>
            Евгений Викторович, руководитель и ведущий врач клиники Пьер Фошар. Его основная
            специализация – это хирургическая и ортопедическая стоматология.
          </p>
          <p>
            Доктор проводит хирургическое лечение любой сложности, установку имплантов,
            костно-пластические операции, на практике применяет технологию All on 4/6...
          </p>
        </>
      ),
    },
    {
      srcSmall: 'img/dist/doctors/2.png',
      src: 'img/dist/doctors/2b.png',
      experience: 15,
      fullName: 'Ирина Александровна Салагина',
      name: 'Ирина',
      surname: 'Салагина',
      middleName: 'Александровна',
      position: '/стоматолог-терапевт/',
      article: (
        <>
          <p>
            Опытный врач-терапевт, владеет всеми современными методиками терапевтического лечения,
            в том числе лечение с применением дентального микроскопа. Регулярно совершенствует свое
            мастерство и повышает квалификацию.
          </p>
        </>
      ),
    },
    {
      srcSmall: 'img/dist/doctors/4.png',
      src: 'img/dist/doctors/4b.png',
      experience: 10,
      fullName: 'Литвинов Павел Николаевич ',
      name: 'Павел',
      surname: 'Литвинов',
      middleName: 'Николаевич',
      position: '/стоматолог-ортопед /стоматолог-терапевт/',
      article: (
        <>
          <p>
            Павел Николаевич, доктор владеет всеми современными видами ортопедического лечения
            зубов. Доктор учитывает все пожелания пациентов и помогает с выбором оптимального
            варианта лечения.
          </p>
          <p>
            Благодаря профессионализму и индивидуальному подходу, выполненные им работы служат
            долгие годы, а пациенты рекомендуют доктора друг другу
          </p>
        </>
      ),
    },
    {
      srcSmall: 'img/dist/doctors/5.png',
      src: 'img/dist/doctors/5b.png',
      experience: 10,
      fullName: 'Будаева Чимита Баторовна ',
      name: 'Чимита',
      surname: 'Будаева',
      middleName: 'Баторовна',
      position: '/стоматолог-ортодонт/',
      article: (
        <>
          <p>
            Чимита Баторовна прекрасный ортодонт сделает ваши зубы ровными и избавит от
            стеснения.
          </p>
          <p>
            Наш ортодонт использует все последние новинки ортодонтии в своем лечении, она имеет
            десятки дипломов и сертификатов по повышению квалификации.
          </p>
        </>
      ),
    },
    {
      srcSmall: 'img/dist/doctors/6.png',
      src: 'img/dist/doctors/6b.png',
      experience: 9,
      fullName: 'Щепина Оксана Евгеньевна',
      name: 'Оксана',
      surname: 'Щепина',
      middleName: 'Евгеньевна',
      position: '/стоматолог-терапевт/',
      article: (
        <>
          <p>Оксана Евгеньевна сделает все, чтобы ваше лечение было качественным и комфортным.</p>
          <p>
            Наш потрясающий врач-терапевт обладает художественным талантом, поэтому будь то
            высокоэстетическая художественная реставрация или обычный кариес, в руках нашего доктора
            станут
            шедеврами лечебного искусства.
          </p>
        </>
      ),
    },
    {
      srcSmall: 'img/dist/doctors/7.png',
      src: 'img/dist/doctors/7b.png',
      experience: 10,
      fullName: 'Александров Игорь Алексеевич ',
      name: 'Игорь',
      surname: 'Александров',
      middleName: 'Алексеевич',
      position: '/стоматолог-ортопед/',
      article: (
        <>
          <p>
            Ортопедическое лечение пациентов по технологии All on 4/6, тотальное съёмное и несъёмное
            протезирование.
          </p>
        </>
      ),
    },
    {
      srcSmall: 'img/dist/doctors/8.png',
      src: 'img/dist/doctors/8b.png',
      experience: 6,
      fullName: 'Шумилов Анатолий Романович ',
      name: 'Анатолий',
      surname: 'Шумилов',
      middleName: 'Романович',
      position: '/ челюстно-лицевой хирург/',
      article: (
        <>
          <p>
            Анатолий Романович, занимается детальной имплантацией, костной и мягкотканой пластикой,
            синус-лифтингом, удалением любой сложности, технологией All on 4/6, тотальным
            протезированием.
          </p>
        </>
      ),
    },
  ];
  const doctor = doctors[(current + 1) % doctors.length];
  return (
    <>
      <TopSection doctor={doctor} />
      <DoctorsListSection doctors={doctors} onChange={(current, next) => setCurrent(next)} />
    </>
  );
};

const TopSection = ({ doctor }) => (
  <section className="doctors" style={{ background: 'url(img/dist/doctors/bg.png) center top/cover' }}>
    <div className="slider_count">
      <p />
      <span />
    </div>
    <div className="doctors_slider">
      {doctor && (
      <DoctorCard
        key={doctor.id}
        experience={doctor.experience}
        src={doctor.src}
        fullName={doctor.fullName}
        position={doctor.position}
        article={doctor.article}
      />
      )}
    </div>
  </section>
);

const ListCard = ({ doctor }) => (
  <div className="doctors_list_slider_elem">
    <img src={doctor.srcSmall} alt={doctor.fullName} />
    <div className="doctors_list_slider_about">
      <h4>{doctor.surname}</h4>
      <p>
        {doctor.name}
      </p>
      <p>
        {doctor.middleName}
      </p>
    </div>
    <a href="#" className="href">Подробнее</a>
  </div>
);

const DoctorsListSection = ({ doctors, onChange }) => {
  const NextArrow = ({ onClick, className, style }) => (
    <div onClick={onClick} className={className} style={style}>
      <ArrowIcon/>
    </div>
  );

  const PrevArrow = ({ onClick, className, style }) => (
    <div onClick={onClick} className={className} style={style}>
      <ArrowIcon/>
    </div>
  );
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    beforeChange: onChange,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
        },
      },
    ],
  };
  return (
    <section className="doctors_list">
      <div className="container">
        <div className="btn blue_btn">Все врачи</div>
      </div>
      <Slider className="doctors_list_slider" {...settings}>
        {doctors.map((e) => (<ListCard doctor={e} />))}
      </Slider>
    </section>
  );
};
