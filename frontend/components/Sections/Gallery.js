import Image from 'next/image';
import { useState } from 'react';
import { ArrowIcon } from '../Icons';

export const GallerySection = () => {
  const cards = [
    { id: 1, url: '/img/dist/gallery/1.webp' },
    { id: 2, url: '/img/dist/gallery/2.webp' },
    { id: 3, url: '/img/dist/gallery/3.webp' },
    { id: 4, url: '/img/dist/gallery/4.webp' },
    { id: 5, url: '/img/dist/gallery/5.webp' },
    { id: 6, url: '/img/dist/gallery/6.webp' },
    { id: 7, url: '/img/dist/gallery/7.webp' },
  ];
  const [current, setCurrent] = useState(1);
  const clickNext = () => setCurrent(current === 7 ? 1 : current + 1);
  const clickPrev = () => setCurrent(current === 1 ? 7 : current - 1);
  return (
    <section className="gallery" >
      <div className="container">
        <div className="gallery_block slider">
          <h2>Улыбки наших пациентов</h2>
          <div className="gallery_text">
            <p className="hash">#улыбки_пьер_фошар</p>
            <button className="btn hidden_href">Хочу в галерею!</button>
          </div>
          <div className={`cards item-${current}--checked`}>
            {cards.map((e) => (
              <label key={e.id} className={`card item-${e.id}`} onClick={() => setCurrent(e.id)}>
                <Image
                  src={e.url}
                  alt={e.alt || 'team'}
                  width={400}
                  height={400}
                />
              </label>
            ))}
            <div className="controls">
              <button type="button" className="previous" onClick={clickPrev}>
                <ArrowIcon />
              </button>
              <button type="button" className="next" onClick={clickNext}>
                <ArrowIcon />
              </button>
            </div>
          </div>
          <div className="gallery_plug">
            <Image src="/img/dist/phone.png" alt="телефон" width={660} height={697} />
          </div>
        </div>
      </div>
    </section>
  );
};
