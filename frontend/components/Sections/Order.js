export const OrderSection = () => (
  <div className="order">
    <div className="map-wrapper">
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1999.9840544174426!2d30.320870316318747!3d59.91581198186784!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46963056907b6813%3A0x5173d9a825a7ffc4!2z0KHQtdGA0L_Rg9GF0L7QstGB0LrQsNGPINGD0LsuLCAxNCwg0KHQsNC90LrRgi3Qn9C10YLQtdGA0LHRg9GA0LMsINCg0L7RgdGB0LjRjywgMTkwMDEz!5e0!3m2!1sru!2sua!4v1626690020970!5m2!1sru!2sua"
        width="100%"
        height="100%"
        style={{ border: '0' }}
        allowFullScreen
        loading="lazy"
      />
    </div>
    <div className="container">
      <div className="order-wrapper">
        <div className="header">
          <div className="title">
            <p>Дадим грамотную консультацию и поможем определиться с лечением</p>
            <h2>Запишитесь прямо сейчас!</h2>
          </div>
          <img src="img/dist/ordering/bg.png" alt="img" />
        </div>
        <form className="order_form">
          <input type="text" className="input" placeholder="Телефон" />
          <button type="submit" className="btn">Записаться!</button>
        </form>
        <span>
          *Нажимая на кнопку, вы даете согласие на обработку своих
          <br />
          персональных данных
        </span>
      </div>
    </div>
  </div>
);
