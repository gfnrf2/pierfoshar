export const PriceSection = () => (
  <>
    <section className="price" style={{ background: 'url(img/dist/price/bg.png) center/contain' }}>
      <div className="container">
        <h2>Стоимость лечения и работ</h2>
        <ul>
          <li>
            <p>Консультация</p>
            <p>от 0р.</p>
          </li>
          <li>
            <p>Кариес</p>
            <p>от 5 000 р.</p>
          </li>
          <li>
            <p>Рентген</p>
            <p>от 1 000 р.</p>
          </li>
          <li>
            <p>Лечение пульпита (эндодонтическое лечение)</p>
            <p>от 10 900 р.</p>
          </li>
          <li>
            <p>Лечение зубов под микроскопом</p>
            <p>от 6 500 р.</p>
          </li>
          <li>
            <p>Лечение пульпита под микроскопом</p>
            <p>от 15 500 р.</p>
          </li>
          <li>
            <p>Профессиональная гигиена полости рта</p>
            <p>от 3 900 р.</p>
          </li>
          <li>
            <p>Отбеливание зубов</p>
            <p>от 23 000 р.</p>
          </li>
          <li>
            <p>Удаление зуба простое</p>
            <p>от 3 000 р.</p>
          </li>
          <li>
            <p>Удаление зуба сложное</p>
            <p>от 5 000 р.</p>
          </li>
          <li>
            <p>Установка импланта</p>
            <p>от 32 000 р.</p>
          </li>
          <li>
            <p>Синус-лифтинг</p>
            <p>от 25 000 р.</p>
          </li>
          <li>
            <p>Пластика уздечки лазером</p>
            <p>от 3 500 р.</p>
          </li>
          <li>
            <p>Коронка металлокерамическая</p>
            <p>от 15 000 р.</p>
          </li>
          <li>
            <p>Коронка диоксид циркония</p>
            <p>от 27 000 р.</p>
          </li>
          <li>
            <p>Коронка E.Max</p>
            <p>от 35 000 р.</p>
          </li>
          <li>
            <p>Устновка брекетов</p>
            <p>от 30 000 р.</p>
          </li>
          <li>
            <p>Десткий кариес</p>
            <p>от 2 500 р.</p>
          </li>
          <li>
            <p>Детский пульпит</p>
            <p>от 4 000 р.</p>
          </li>
          <li>
            <p>Детская профгигиена</p>
            <p>от 2 500 р.</p>
          </li>
        </ul>
        <button className="btn sing_up">Записаться!</button>
      </div>
    </section>

    <div className="credit">
      <div className="container">
        <div className="credit_block" >
          <img src="img/dist/credit.svg" />
          <div className="plug">
            <div className="title">
              <h2>Рассрочка под 0% от банка</h2>
              <p>Помогаем с получением рассрочки даже в сложных случаях</p>
            </div>
            <button className="btn sing_up">Записаться!</button>
          </div>
        </div>
      </div>
    </div>
  </>
);
