import Image from 'next/image';

export const PromoSection = () => (
  <section className="promo" style={{ background: 'url(img/dist/promo/bg.png) center/cover' }}>
    <div className="team-bg">
      <Image src="/img/dist/promo/team.png" width={950} height={650} alt="Профессиональная команда" />
    </div>
    <div className="container">
      <div className="block">
        <h1>
          Команда профессионалов
          <span>в клинике Пьер Фошар</span>
        </h1>
        <div className="promo_list">
          <ul>
            <li>
              <span className="circle" />
              <span className="orange">3 года гарантии</span>
              на лечение зубов
            </li>
            <li>
              <span className="circle" />
              <span className="orange" style={{ display: 'inline' }}>цифровая </span>
              стоматология
            </li>
            <li>
              <span className="circle" />
              <span className="orange" style={{ display: 'inline' }}>0% </span>
              рассрочка
            </li>
            <li>
              <span className="circle" />
              лечение зубов&nbsp;
              <span className="orange"> под микроскопом</span>
            </li>
          </ul>
          <div className="promo-microscope" >
            <div className="bg" />
            <Image src="/img/dist/promo/micro.png" width={220} height={230} alt="Микроскоп" className="promo_list_img"  />
          </div>
        </div>
        <div className="plug">
          <h2>
            <span style={{ display: 'flex' }}>
              Получите
              <span className="orange"> бесплатно</span>
            </span>
            консультацию
          </h2>
          <form className="promo_form">
            <input type="text" className="input" id="phone1" placeholder="Телефон" required />
            <button type="button" className="btn">Записаться!</button>
          </form>
          <span className="info">* Нажимая на кнопку, вы даете согласие на обработку своих персональных данных</span>
        </div>
      </div>
    </div>
  </section>
);
