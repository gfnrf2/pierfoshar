export const RatioSection = () => (
    <section className="ratio">
        <div className="container">
            <div className="ratio_block">
                <img src="img/dist/raiting/hand.png" className="ratio_img" />
                <div className="title">
                    <h2>Независимые рейтинги</h2>
                    <p>
                        Репутация клиники и стоматологов – важный критерий при выборе места лечения.
                        <br />
                        Узнайте, что думают пациенты о клинике Пьер Фошар на популярных интернет порталах
                    </p>
                </div>
                <div className="ratio_content">
                    <a href="https://maps.app.goo.gl/XzxhPYrgU4M7vnpR6" target="_blank" className="ratio_elem" rel="noreferrer">
                        <img src="img/dist/raiting/google.png" alt="img" />
                        <div className="ratio_score">
                            <p>
                                5
                                <span>/5</span>
                            </p>
                        </div>
                    </a>
                    <a
                        href="https://yandex.ru/maps/org/pyer_foshar/1110201438/?ll=30.322911%2C59.915867&z=17"
                        target="_blank"
                        className="ratio_elem"
                        rel="noreferrer"
                    >
                        <img src="img/dist/raiting/ya.png" alt="img" />
                        <div className="ratio_score">
                            <p>
                                4.5
                                <span>/5</span>
                            </p>
                        </div>
                    </a>
                    <a
                        href="https://spb.zoon.ru/medical/stomatologicheskaya_klinika_per_foshar_na_serpuhovskoj_ulitse/"
                        target="_blank"
                        className="ratio_elem"
                        rel="noreferrer"
                    >
                        <img src="img/dist/raiting/zq.png" alt="img" />
                        <div className="ratio_score">
                            <p>
                                4.2
                                <span>/5</span>
                            </p>
                        </div>
                    </a>
                    <a href="https://2gis.ru/spb/firm/5348552838790587" target="_blank" className="ratio_elem" rel="noreferrer">
                        <img src="img/dist/raiting/2gis.png" alt="img" />
                        <div className="ratio_score">
                            <p>
                                4.7
                                <span>/5</span>
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
);