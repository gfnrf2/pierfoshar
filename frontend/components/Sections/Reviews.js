import { useState } from 'react';
import Image from 'next/image';
import Slider from 'react-slick';
import { ArrowIcon } from '../Icons';

export const ReviewsSection = () => {
  const [current, setCurrent] = useState(0);
  const NextArrow = (props) => (
    <div onClick={props.onClick} className={props.className} style={props.style}>
      <ArrowIcon/>
    </div>
  );

  const PrevArrow = (props) => (
    <div onClick={props.onClick} className={props.className} style={props.style}>
      <ArrowIcon/>
    </div>
  );
  const settings = {
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    autoplay: true,
    autoplaySpeed: 5000,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    slidesToScroll: 1,
    beforeChange: (_, next) => setCurrent(next),
  };
  const reviews = [
    {
      id: 0,
      key: 'qA5fNQ-CkcQ',
    },
    {
      id: 1,
      key: '2C1pV-TCOp8',
    }, {
      id: 2,
      key: 'mF1srdFm-4w',
    },
    {
      id: 3,
      key: '24aakasz_xM',
    },
    {
      id: 4,
      key: 'xAqoNm_w1Hc',
    },
    {
      id: 5,
      key: 'p4kJUONKl_E',
    },
    {
      id: 6,
      key: 'Scp6LAFgS2E',
    },
    {
      id: 7,
      key: 'cLrRhMRL2NA',
    },
    {
      id: 8,
      key: 'EMtBUsgVtRw',
    },
    {
      id: 9,
      key: 'lvVR07Lpy3I',
    },
    {
      id: 10,
      key: 'YTwTWBcVJzM',
    },
    {
      id: 11,
      key: 'l6z1l349Hmo',
    },
    {
      id: 13,
      key: 'wED6_XeMW4I',
    },
    {
      id: 14,
      key: '5Y2NVervtRA',
    },
  ];

  return (
    <section className="reviews" style={{ background: 'url(img/dist/reviews-bg.png) center/cover' }}>
      <div className="container">
        <div className="reviews_wrap">
          <div className="reviews_block">
            <div className="reviews_title">
              <p className="percent">98%</p>
              <p className="text">
                пациентов
                <br />
                <span>рекомендуют клинику</span>
              </p>
            </div>
            <Image src="/img/dist/pier.svg" height={114} width={312} alt="Пьер Фошар" />
            <form className="reviews_form" id="form2">
              <input type="text" className="input" id="phone2" placeholder="Телефон" />
              <button type="button" className="btn">Записаться!</button>
              <span className="info">
                *Нажимая на кнопку, вы даете согласие на обработку своих персональных данных
              </span>
            </form>
          </div>
          <div className="slider_count">
            <p />
            <span />
          </div>
          <div className="reviews_content">
            <div className="review_slider">
              <div className="for_video ">
                <iframe
                  width="99.9%"
                  height="262px"
                  src={`https://www.youtube.com/embed/${reviews[(current + 1) % reviews.length].key}`}
                  frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen
                />
              </div>
              <Slider {...settings} className="reviews_slider_navigation">
                {reviews.map((e) => (
                  <div className="reviews_img">
                    <img key={e.id} src={`//img.youtube.com/vi/${e.key}/maxresdefault.jpg`} alt="Видео на YouTube"/>
                  </div>
                ))}
              </Slider>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
