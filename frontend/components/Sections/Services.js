import services from '../../data/services';

export const ServicesSection = () => (
  <section className="services">
    <div className="container">
      <h2>Наши услуги</h2>
      <div className="services_block">
        {services.map((e) => (
          <div className="item" key={e.id}>
            <img {...e.img} />
            <h3>{e.title}</h3>
            {e.list && (
              <ol className="services_block_ol">
                {e.list.map((item, i) => (<li key={i}>{item}</li>))}
              </ol>
            )}
            <a className="hidden_href" href={e.href}>/ Подробнее /</a>
          </div>
        ))}
      </div>
    </div>
  </section>
);
