import { useState } from 'react';
import Image from 'next/image';
import { ArrowIcon } from '../Icons';

export const TeamSection = () => {
  const cards =[
    { id: 1, url: '/img/dist/team/1.png' },
    { id: 2, url: '/img/dist/team/2.png' },
    { id: 3, url: '/img/dist/team/3.png' },
    { id: 4, url: '/img/dist/team/4.png' },
    { id: 5, url: '/img/dist/team/5.png' },
    { id: 6, url: '/img/dist/team/6.png' },
    { id: 7, url: '/img/dist/team/7.png' },
  ];
  const [current, setCurrent] = useState(1);
  const clickNext = () => setCurrent(current === 7 ? 1 : current + 1);
  const clickPrev = () => setCurrent(current === 1 ? 7 : current - 1);

  return (
    <>
      <section className="team" style={{ background: 'url(img/dist/team/bg.svg) center/contain' }}>
        <h2>Мы – Пьер Фошар</h2>
        <div className="slider-wrapper">
          <div className="container slider">
            <div className={`cards item-${current}--checked`}>
              {cards.map((e) => (
                <label key={e.id} className={`card item-${e.id}`} onClick={() => setCurrent(e.id)}>
                  <Image
                    src={e.url}
                    alt={e.alt || 'team'}
                    width={500}
                    height={500}
                  />
                </label>
              ))}
            </div>
          </div>
          <div className="controls">
            <button type="button" className="previous" onClick={clickPrev}>
              <ArrowIcon />
            </button>
            <button type="button" className="next" onClick={clickNext}>
              <ArrowIcon />
            </button>
          </div>
        </div>
      </section>
    </>
  );
};
