export const WorkSection = () => (
  <section className="work" style={{ background: 'url(img/dist/work/bg.png) center right' }}>
    <div className="container">
      <div className="work_block">
        <div className="title">
          <h2>Наши принципы работы</h2>
          <p>
            После тщательной диагностики и планирования, назначается день операции по установки имплантатов в
            полость рта.
          </p>
        </div>
        <div className="work_content">
          <div className="principal-card">
            <img src="img/dist/work/2.png" />
            <h5>Относимся к пациентам с уважением и любовью</h5>
            <p>
              лечим каждого так, как
              лечили бы самого близкого
              человека
            </p>
          </div>
          <div className="principal-card">
            <img src="img/dist/work/4.png" />
            <h5>Заботимся о здоровье наших пациентов</h5>
            <p>
              используем только
              высококачественные
              материалы
            </p>
          </div>
          <div className="principal-card">
            <img src="img/dist/work/3.png" />
            <h5>Предлагаем лучшие из возможных методов лечения</h5>
            <p>
              применяем все
              инновационные технологии в
              области стоматологии
            </p>
          </div>
          <div className="principal-card">
            <img src="img/dist/work/1.png" />
            <h5>Совместно с пациентом выбираем план лечения</h5>
            <p>
              объясняем проблемы и
              возможности их решения
              понятным языком
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
);
