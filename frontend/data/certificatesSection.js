const certificatesSection = {
  title: 'Сертификаты',
  description: 'Специалисты клиники Пьер Фошар имеют высокую квалификацию, прошли обучение в\nРоссии и за рубежом, имеют многочисленные сертификаты по современным методам\nвосстановления зубов, в том числе по имплантации и протезированию зубов.',
  certs: [
    {
      id: 0,
      url: '',
      img: {
        src: '/img/dist/certs/1.png',
        width: 2048,
        height: 1392,
        alt: 'cert',
      },
    },
    {
      id: 1,
      url: '',
      img: {
        src: '/img/dist/certs/2.png',
        width: 2320,
        height: 1660,
        alt: 'cert',
      },
    },
    {
      id: 2,
      url: '',
      img: {
        src: '/img/dist/certs/3.png',
        width: 1665,
        height: 2340,
        alt: 'cert',
      },
    },
    {
      id: 3,
      url: '',
      img: {
        src: '/img/dist/certs/4.png',
        width: 2336,
        height: 1245,
        alt: 'cert',
      },
    },
    {
      id: 4,
      url: '',
      img: {
        src: '/img/dist/certs/5.png',
        width: 2336,
        height: 1642,
        alt: 'cert',
      },
    },
    {
      id: 5,
      url: '',
      img: {
        src: '/img/dist/certs/6.png',
        width: 1660,
        height: 2340,
        alt: 'cert',
      },
    },
    {
      id: 6,
      url: '',
      img: {
        src: '/img/dist/certs/7.png',
        width: 2256,
        height: 1602,
        alt: 'cert',
      },
    },
    {
      id: 7,
      url: '',
      img: {
        src: '/img/dist/certs/8.png',
        width: 2320,
        height: 1346,
        alt: 'cert',
      },
    },
    {
      id: 8,
      url: '',
      img: {
        src: '/img/dist/certs/9.png',
        width: 2320,
        height: 1634,
        alt: 'cert',
      },
    },
    {
      id: 9,
      url: '',
      img: {
        src: '/img/dist/certs/10.png',
        width: 1700,
        height: 2333,
        alt: 'cert',
      },
    },
    {
      id: 10,
      url: '',
      img: {
        src: '/img/dist/certs/11.png',
        width: 2320,
        height: 1700,
        alt: 'cert',
      },
    },
    {
      id: 11,
      url: '',
      img: {
        src: '/img/dist/certs/12.png',
        width: 1700,
        height: 2340,
        alt: 'cert',
      },
    },
    {
      id: 12,
      url: '',
      img: {
        src: '/img/dist/certs/13.png',
        width: 2336,
        height: 1651,
        alt: 'cert',
      },
    },
    {
      id: 13,
      url: '',
      img: {
        src: '/img/dist/certs/14.png',
        width: 1651,
        height: 2325,
        alt: 'cert',
      },
    },
    {
      id: 14,
      url: '',
      img: {
        src: '/img/dist/certs/15.png',
        width: 1627,
        height: 2330,
        alt: 'cert',
      },
    },
    {
      id: 15,
      url: '',
      img: {
        src: '/img/dist/certs/16.png',
        width: 2320,
        height: 1629,
        alt: 'cert',
      },
    },
    {
      id: 16,
      url: '',
      img: {
        src: '/img/dist/certs/17.png',
        width: 2256,
        height: 1640,
        alt: 'cert',
      },
    },
  ],
};

export default certificatesSection;
