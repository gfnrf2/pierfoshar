import {FacebookIcon, InstagramIcon, WhatsappIcon, YouTubeSocIcon} from "../components/Icons";

const contacts = {
  email: 'pjerfoshar@yandex.ru',
  phone: '+7 (812) 309-06-62',
  company: 'ООО «Пьер Фошар»',
  inn: 'ИНН 7838329727',
  address: 'г. Санкт-Петербург, Ст. м. Технологический\nинститут, Серпуховская улица, 14',
  wordTimes: [
    'ПН-ПТ: с 10 до 20',
    'СБ: с 10 до 19',
  ],
  social: [
    {
      id: 0,
      name: 'Instagram',
      link: 'https://www.instagram.com/foshar_dental_clinic/?igshid=kn55ut2ecanq',
      icon: (<InstagramIcon />),
    },
    {
      id: 1,
      name: 'Facebook',
      link: 'https://www.instagram.com/foshar_dental_clinic/?igshid=kn55ut2ecanq',
      icon: (<FacebookIcon />),
    },
    {
      id: 2,
      name: 'YouTube',
      link: 'https://www.instagram.com/foshar_dental_clinic/?igshid=kn55ut2ecanq',
      icon: (<YouTubeSocIcon />),
    },
    {
      id: 3,
      name: 'Whatsapp',
      link: 'https://www.instagram.com/foshar_dental_clinic/?igshid=kn55ut2ecanq',
      icon: (<WhatsappIcon />),
    },
  ],
};

export default contacts;
