const menuItems = [
  { id: 0, title: 'Услуги', href: '/services' },
  { id: 1, title: 'Врачи', href: '/doctors' },
  { id: 2, title: 'Цены', href: '/price' },
  { id: 3, title: 'Акции', href: '/sales' },
  { id: 4, title: 'Премиум', href: '/premium' },
  { id: 5, title: 'Клиника', href: '/clinic' },
  { id: 6, title: 'Отзывы', href: '/reviews' },
  { id: 7, title: 'Портфолио', href: '/portfolio' },
  { id: 8, title: 'Контакты', href: '/contacts' },
];

export default menuItems;
