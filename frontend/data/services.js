const services = [
  {
    id: 0,
    title: 'Лечение зубов',
    img: {
      src: 'img/dist/services/1.png',
      alt: '',
    },
    href: '',
    list: [
      'Лечение кариеса',
      'Лечение пульпита',
      'Лечение болезни десен',
    ],
  },
  {
    id: 1,
    title: 'Протезирование зубов',
    img: {
      src: 'img/dist/services/2.png',
      alt: '',
    },
    href: '',
  },
  {
    id: 2,
    title: 'Хирургическая стоматология',
    img: {
      src: 'img/dist/services/3.png',
      alt: '',
    },
    href: '',
  },
  {
    id: 3,
    title: 'Имплантация',
    img: {
      src: 'img/dist/services/4.png',
      alt: '',
    },
    href: '',
  },
  {
    id: 4,
    title: 'Ортодонтия',
    img: {
      src: 'img/dist/services/5.png',
      alt: '',
    },
    href: '',
  },
  {
    id: 5,
    title: 'Гигиена и профилактика',
    img: {
      src: 'img/dist/services/6.png',
      alt: '',
    },
    href: '',
  },
  {
    id: 6,
    title: 'Стоматология для детей',
    img: {
      src: 'img/dist/services/7.png',
      alt: '',
    },
    href: '',
  },
  {
    id: 7,
    title: 'Все зубы за 1 день',
    img: {
      src: 'img/dist/services/1.png',
      alt: '',
    },
    href: '',
  },
  {
    id: 8,
    title: 'Цифровая стоматология',
    img: {
      src: 'img/dist/services/2.png',
      alt: '',
    },
    href: '',
  },
  {
    id: 9,
    title: 'Диагностика',
    img: {
      src: 'img/dist/services/3.png',
      alt: '',
    },
    href: '',
  },
];

export default services;
