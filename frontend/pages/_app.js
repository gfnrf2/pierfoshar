import 'normalize.css/normalize.css';
import '../styles/scss/styles.css';

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default MyApp;
