import { useState } from 'react';
import { send } from 'emailjs-com';
import { Header } from '../components/Layout/Header';
import { Footer } from '../components/Layout/Footer';
import { PromoSection } from '../components/Sections/Promo';
import { ClinicSecondSection, ClinicSection } from '../components/Sections/Clinic';
import { ServicesSection } from '../components/Sections/Services';
import { TeamSection } from '../components/Sections/Team';
import { AppealSection } from '../components/Sections/Appeal';
import { AnswersSection } from '../components/Sections/Answers';
import { PriceSection } from '../components/Sections/Price';
import { ReviewsSection } from '../components/Sections/Reviews';
import { WorkSection } from '../components/Sections/Work';
import { RatioSection } from '../components/Sections/Ratio';
import { GallerySection } from '../components/Sections/Gallery';
import { FaqSection } from '../components/Sections/Faq';
import { OrderSection } from '../components/Sections/Order';
import { AdvantagesSection } from '../components/Sections/Advantages';
import { DoctorsSection } from '../components/Sections/Doctors';
import { CertificatesSection } from '../components/Sections/Certificates';

const EmailForm = () => {
  const [toSend, setToSend] = useState({
    from_name: '',
    to_name: '',
    message: '',
    reply_to: '',
  });

  const onSubmit = (e) => {
    e.preventDefault();
    send(
      'service_xczydmd',
      'template_a15fgfg',
      toSend,
      'user_f4oUqOJ7lie2tJzjZVhtL',
    )
      .then((response) => {
        console.log('SUCCESS!', response.status, response.text);
      })
      .catch((err) => {
        console.log('FAILED...', err);
      });
  };

  const handleChange = (e) => {
    setToSend({ ...toSend, [e.target.name]: e.target.value });
  };
  return (
    <form onSubmit={onSubmit} className="modal_scroll feedback_form" >
      <h4>Обратный звонок</h4>
      <p>Введите свои данные и мы Вам перезвоним</p>
      <input
        type="text"
        name="from_name"
        className="input"
        placeholder="Имя"
        value={toSend.from_name}
        onChange={handleChange}
        required
      />
      <input
        type="tel"
        name="message"
        value={toSend.message}
        onChange={handleChange}
        className="input"
        placeholder="Телефон"
        required
      />
      <button type="submit" className="btn">Перезвоните мне</button>
      <span className="info_span">*Нажимая на кнопку, вы даете согласие на обработку своих персональных данных</span>
    </form>
  );
};

const ContactForm = () => {
  const [toSend, setToSend] = useState({
    from_name: '',
    to_name: '',
    message: '',
    reply_to: '',
  });

  const onSubmit = (e) => {
    e.preventDefault();
    send(
      'service_xczydmd',
      'template_a15fgfg',
      toSend,
      'user_f4oUqOJ7lie2tJzjZVhtL',
    )
      .then((response) => {
        alert('Успешно');
      })
      .catch((err) => {
        alert('Что-то пошло не так...');
      });
  };

  const handleChange = (e) => {
    setToSend({ ...toSend, [e.target.name]: e.target.value });
  };
  return (
    <form onSubmit={onSubmit} className="modal_scroll feedback_form" >
      <h4>Обратный звонок</h4>
      <p>Введите свои данные и мы Вам перезвоним</p>
      <input
        type="text"
        name="from_name"
        className="input"
        placeholder="Имя"
        value={toSend.from_name}
        onChange={handleChange}
        required
      />
      <input
        type="tel"
        name="message"
        value={toSend.message}
        onChange={handleChange}
        className="input"
        placeholder="Телефон"
        required
      />
      <button type="submit" className="btn">Перезвоните мне</button>
      <span className="info_span">*Нажимая на кнопку, вы даете согласие на обработку своих персональных данных</span>
    </form>
  );
};

export default function HomePage() {
  const [modal, setModal] = useState(false);
  return (
    <>
      <Header onClick={() => setModal(true)} />

      <PromoSection />

      <ClinicSection />

      <AppealSection />

      <TeamSection />

      <ServicesSection />

        {/* <AdvantagesSection /> */}

      <DoctorsSection />

      <CertificatesSection />

      <ClinicSecondSection />

      <AnswersSection />

      <PriceSection />

        {/* <ReviewsSection /> */}

      <WorkSection />

      <RatioSection />

        {/*  <GallerySection /> */}

      <FaqSection />

      <OrderSection />

      <Footer />

      <div className="modal_wrap" style={modal ? { display: 'block' } : { display: 'none' }}>
        <div className="modal feedback_modal">
          <div className="close" onClick={() => setModal(false)} >
            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd" clipRule="evenodd" d="M14.4962 1.06065C15.082 1.64643 15.082 2.59618 14.4962 3.18197L3.18252 14.4957C2.59673 15.0815 1.64698 15.0815 1.06119 14.4957C0.475409 13.9099 0.475409 12.9601 1.0612 12.3744L12.3749 1.06065C12.9607 0.474861 13.9104 0.474861 14.4962 1.06065Z"/>
              <path fillRule="evenodd" clipRule="evenodd" d="M1.06068 1.06087C1.64646 0.475087 2.59621 0.475087 3.182 1.06087L14.4957 12.3746C15.0815 12.9604 15.0815 13.9101 14.4957 14.4959C13.9099 15.0817 12.9602 15.0817 12.3744 14.4959L1.06068 3.18219C0.474891 2.59641 0.474891 1.64666 1.06068 1.06087Z"/>
            </svg>
          </div>
          <EmailForm />
        </div>
        <div className="modal successes">
          <img src="img/done.svg" alt="img" />
          <h3>Спасибо</h3>
          <p>Ваша заявка отправлена, мы перезвоним</p>
        </div>
      </div>
    </>
  );
}
